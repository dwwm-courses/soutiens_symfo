<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre')
            ->add('contenu')
            ->add('category', ChoiceType::class, [
                "choices"=> [
                    "Sport"=> "Sport",
                    "Faits divers"=> "Faits divers",
                    "Politique"=> "Politique",
                    "Santé"=> "Santé"
                ]
            ])

            ->add('save', SubmitType::class, [
                'label' => 'Create Task',
                "attr"=> [
                    "class"=> "btn btn-success mt-2"
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
