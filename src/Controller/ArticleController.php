<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Form\SearchType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/', name: 'app_article')]
    public function index(ArticleRepository $articleRepository, Request $request): Response
    {
        $articles = [];

        $searchForm = $this->createForm(SearchType::class);
        $searchForm->handleRequest($request);

        if($searchForm->isSubmitted() && $searchForm->isValid()){
            $filters = $searchForm->getData();

            $articles = $articleRepository->search($filters);

        } else {
            $articles =  $articleRepository->findAll();
        }

        return $this->render('article/index.html.twig', [
            'articles'=> $articles,
            'searchForm'=> $searchForm->createView()
        ]);
    }

    #[Route('/ajout', name: 'app_ajout_article')]
    public function ajout(Request $request, EntityManagerInterface $em): Response
    {

        $form = $this->createForm(ArticleType::class, new Article());
        $form = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $article = $form->getData();
            $article->setDateAjout(new \DateTime());
           $em->persist($article);
           $em->flush();

           return $this->redirectToRoute("app_article");
        }

        return $this->render('article/ajout.html.twig', [
            "form"=> $form->createView()
        ]);
    }
}
