<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Article>
 *
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function search($filters){
        $query = $this->createQueryBuilder('a');

        if(!empty($filters["searchBar"])){
            $query->andWhere('a.titre LIKE :titre OR a.contenu LIKE :contenu OR a.category LIKE :category')
            ->setParameter(":titre", "%".$filters["searchBar"]."%")
                ->setParameter(":contenu", "%".$filters["searchBar"]."%")
                ->setParameter(":category", "%".$filters["searchBar"]."%");
        }

        if(!is_null($filters["category"])){
            $query->andWhere("a.category = :categorySelect")
                ->setParameter("categorySelect", $filters["category"]);
        }

        if(!is_null($filters["dateAdd"])){
            $query->andWhere("a.dateAjout = :dateAjout")
                ->setParameter("dateAjout", $filters["dateAdd"]);
        }

        return $query->getQuery()->getResult();
    }
//    /**
//     * @return Article[] Returns an array of Article objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Article
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
